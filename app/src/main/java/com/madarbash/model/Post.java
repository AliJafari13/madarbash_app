package com.madarbash.model;

import android.text.Html;
import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.madarbash.SolarCalendar;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by ali on 4/22/2017.
 */

public class Post {

    public Post(JsonObject jsonObject) {
        setId(jsonObject.get("id").getAsInt());

        try {
            SimpleDateFormat datetimeFormatter1 = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
            Date lFromDate1 = datetimeFormatter1.parse(jsonObject.get("date").getAsString());
            Timestamp fromTS1 = new Timestamp(lFromDate1.getTime());
            setDate(fromTS1.getTime());
        }catch (ParseException e){
            Log.e("dffgfdg",e.getMessage(),e);
            Date lFromDate1 = new Date();
            setDate(lFromDate1.getTime());
        }

        setLink(jsonObject.get("link").getAsString());
        setTitle(jsonObject.get("title").getAsJsonObject().get("rendered").getAsString());
        setContent(jsonObject.get("content").getAsJsonObject().get("rendered").getAsString());
        //setExcerpt(jsonObject.get("excerpt").getAsJsonObject().get("rendered").getAsString());
        setExcerpt(Html.fromHtml(jsonObject.get("excerpt").getAsJsonObject().get("rendered").getAsString()).toString());
        setFeaturedMedia(jsonObject.get("featured_media").getAsInt());
        setCategories(jsonObject.get("categories").getAsJsonArray());
        setTags(jsonObject.get("tags").getAsJsonArray());
    }

    private int id;
    private Long date;
    private String Jdate;
    private String link;
    private String title;
    private String content;
    private String raw_content;
    private String excerpt;
    private int featured_media;
    private JsonArray categories;
    private JsonArray tags;

    public int getId() {
        return id;
    }

    public Long getDate() {
        return date;
    }

    public String getJdate() {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(date);
        SolarCalendar s = new SolarCalendar(c);
        this.Jdate = s.getDateFormat();
        return Jdate;
    }

    public String getAgoDate() {
        return getTimeAgo(getDate());
    }

    public String getLink() {
        return link;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public String getRawContent() {
        if (raw_content == null)
            raw_content = Html.fromHtml(this.getContent()).toString();
        return raw_content;
    }

    public String getExcerpt() {
        return excerpt;
    }

    public int getFeaturedMedia() {
        return featured_media;
    }

    public JsonArray getCategories() {
        return categories;
    }

    public JsonArray getTags() {
        return tags;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setExcerpt(String excerpt) {
        this.excerpt = excerpt;
    }

    public void setFeaturedMedia(int featured_media) {
        this.featured_media = featured_media;
    }

    public void setCategories(JsonArray categories) {
        this.categories = categories;
    }

    public void setTags(JsonArray tags) {
        this.tags = tags;
    }

    public static String getTimeAgo(long time) {
        if (time < 1000000000000L) {
            //time *= 1000;
        }
        time /= 1000;
        Date d = new Date();
        long now = d.getTime()/1000;
        if (time > now || time <= 0) {
            return null;
        }
        final long diff = now - time;
        if (diff < 60) {
            return "لحظه ای پیش";
        } else if (diff < 2 * 60) {
            return "یک دقیقه پیش";
        } else if (diff < 50 * 60) {
            return diff / 60 + " دقیقه قبل";
        } else if (diff < 90 * 60) {
            return "یک ساعت قبل";
        } else if (diff < 24 * 3600) {
            return diff / 3600 + " ساعت قبل";
        } else if (diff < 48 * 3600) {
            return "دیروز";
        } else {
            return diff / 86400 + " روز قبل";
        }
    }
}
