package com.madarbash.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.madarbash.ApiInterface;
import com.madarbash.CardAdapter;
import com.madarbash.MadarbashApplication;
import com.madarbash.R;
import com.madarbash.model.Post;

import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private Tracker mTracker;
    private Activity activity;
    public boolean FirstFeedGotten = false;
    private int per_page = 20;
    public int page = 1;
    public ApiInterface service;
    public int network_status = 0;
    private BroadcastReceiver broadcastReceiver;
    private CardAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    public SwipeRefreshLayout swipeRefreshLayout;
    boolean loading = true;
    boolean scroll_post_finished = false;
    private ArrayList mItems = new ArrayList<Post>();

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        activity = this;
        mTracker = ((MadarbashApplication) activity.getApplication()).getDefaultTracker();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(((MadarbashApplication) activity.getApplication()).base_url).addConverterFactory(GsonConverterFactory.create()).build();
        service = retrofit.create(ApiInterface.class);

        broadcastReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                if (null != activeNetwork) {
                    if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                        network_status = 1;
                    if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                        network_status = 2;
                }else {
                    network_status = 0;
                    Snackbar.make(activity.findViewById(android.R.id.content), "اتصال به اینترنت برقرار نیست.", Snackbar.LENGTH_LONG).show();
                }
                if (network_status > 0)
                    getFirstFeeds();
                Log.e("network receive", ""+network_status);
            }
        };
        activity.registerReceiver(broadcastReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_red_light , android.R.color.holo_orange_light, android.R.color.holo_blue_bright, android.R.color.holo_green_light);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                FirstFeedGotten = false;
                scroll_post_finished = false;
                getFirstFeeds();
            }
        });
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    public void setScrollListener(){
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            int pastVisiblesItems, visibleItemCount, totalItemCount;
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && !scroll_post_finished) {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if ((visibleItemCount + pastVisiblesItems + 5) >= totalItemCount) {
                        if (loading) {
                            loading = false;
                            swipeRefreshLayout.setRefreshing(true);

                            Call<JsonArray> call = service.post_get_list(page,per_page);
                            call.enqueue(new retrofit2.Callback<JsonArray>() {
                                @Override
                                public void onResponse(Call<JsonArray> call, retrofit2.Response<JsonArray> r) {
                                    if(r.isSuccessful()){
                                        JsonArray feeds = r.body().getAsJsonArray();
                                        if (feeds.size() == 0) {
                                            scroll_post_finished = true;
                                            Snackbar.make(findViewById(android.R.id.content), "مطلب بیشتری وجود ندارد :(", Snackbar.LENGTH_SHORT).show();
                                        }else {
                                            for (int i = 0; i < feeds.size(); i++) {
                                                Post mFeed = new Post(feeds.get(i).getAsJsonObject());
                                                mItems.add(mFeed);
                                            }
                                            mRecyclerView.swapAdapter(mAdapter, false);
                                            page++;
                                        }
                                    }else {
                                        Snackbar.make(findViewById(android.R.id.content), "مشکلی در برقراری ارتباط با سرور پیش آمده!", Snackbar.LENGTH_SHORT).show();
                                    }
                                    loading = true;
                                    swipeRefreshLayout.setRefreshing(false);
                                }
                                @Override
                                public void onFailure(Call<JsonArray> call, Throwable t) {
                                    swipeRefreshLayout.setRefreshing(false);
                                    Snackbar.make(findViewById(android.R.id.content), "مشکلی در برقراری ارتباط با سرور پیش آمده!", Snackbar.LENGTH_SHORT).show();
                                    loading = true;
                                }
                            });
                        }
                    }
                }
            }
        });
    }

    public void getFirstFeeds(){
        if (!FirstFeedGotten && loading){
            swipeRefreshLayout.setRefreshing(true);
            loading = false;
            page = 1;
            mItems.clear();
            Call<JsonArray> call = service.post_get_list(page,per_page);
            call.enqueue(new retrofit2.Callback<JsonArray>() {
                @Override
                public void onResponse(Call<JsonArray> call, retrofit2.Response<JsonArray> r) {
                    if(r.isSuccessful()) {
                        JsonArray feeds = r.body().getAsJsonArray();
                        for (int i = 0; i < feeds.size(); i++) {
                            Post post = new Post(feeds.get(i).getAsJsonObject());
                            mItems.add(post);
                        }
                        mAdapter = new CardAdapter(activity,mItems);
                        mRecyclerView.setAdapter(mAdapter);

                        setScrollListener();
                        FirstFeedGotten = true;
                        page++;
                    }else {
                        Toast.makeText(activity,r.message(),Toast.LENGTH_SHORT).show();
                        Log.e("JsonSyntaxException","isSuccessful false");
                    }
                    loading = true;
                    swipeRefreshLayout.setRefreshing(false);
                }
                @Override
                public void onFailure(Call<JsonArray> call, Throwable t) {
                    loading = true;
                    Log.e("feed_get_dashboard_feed",t.getMessage(),t);
                    swipeRefreshLayout.setRefreshing(false);
                }
            });
        }else {
            swipeRefreshLayout.setRefreshing(false);
            Log.e("fghgfhj",FirstFeedGotten+" "+loading);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        mTracker.setScreenName("Dashboard Screen");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_reload) {
            FirstFeedGotten = false;
            getFirstFeeds();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.contact_us) {
            AlertDialog inviteAlertDialog = new AlertDialog.Builder(activity)
                    .setTitle("ارتباط با ما")
                    .setMessage("هرگونه انتقاد، پیشنهاد و نظر خود را به ایمیل info@madarbash.com ارسال نمایید.")
                    .setNegativeButton("خُب", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .create();

            inviteAlertDialog.show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return false;
    }
}
