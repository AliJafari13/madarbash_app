package com.madarbash;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.madarbash.activity.PostViewActivity;
import com.madarbash.model.Post;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ali on 4/22/2017.
 */

public class CardAdapter extends RecyclerView.Adapter<CardAdapter.BaseViewHolder> {
    List<Post> mItems;
    Activity mContext;


    public CardAdapter(Activity context , List<Post> Items) {
        super();
        mContext = context;
        mItems = Items;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        BaseViewHolder holder;

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_view_post_card, viewGroup, false);
        holder = new FeedViewHolder(v);

        return holder;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder base_viewHolder, int i) {
        final Post feed = mItems.get(i);
        final FeedViewHolder viewHolder = (FeedViewHolder) base_viewHolder;

        viewHolder.title.setText(feed.getTitle());
        viewHolder.excerpt.setText(feed.getExcerpt());

        viewHolder.date.setText(feed.getAgoDate());

        if (feed.getFeaturedMedia() > 0){
            Retrofit retrofit = new Retrofit.Builder().baseUrl(((MadarbashApplication) mContext.getApplication()).base_url).addConverterFactory(GsonConverterFactory.create()).build();
            ApiInterface service = retrofit.create(ApiInterface.class);
            Call<JsonObject> call = service.media_get(feed.getFeaturedMedia());
            call.enqueue(new retrofit2.Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> r) {
                    if(r.isSuccessful()) {
                        Picasso.with(mContext).load(r.body().get("guid").getAsJsonObject().get("rendered").getAsString()).into(viewHolder.image);
                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {}
            });
        }else
            viewHolder.image.setVisibility(View.GONE);

        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(mContext,"click on "+feed.getId(),Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(mContext, PostViewActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt("id", feed.getId());
                intent.putExtras(bundle);
                mContext.startActivity(intent);
                mContext.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    class BaseViewHolder extends RecyclerView.ViewHolder{
        public CardView cardView;
        public BaseViewHolder(View itemView) {
            super(itemView);
        }
    }

    class FeedViewHolder extends BaseViewHolder{
        private TextView title;
        private TextView excerpt;
        private ImageView image;
        private TextView date;

        public FeedViewHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.cardView);
            image = (ImageView)itemView.findViewById(R.id.image);
            title = (TextView)itemView.findViewById(R.id.title);
            excerpt = (TextView)itemView.findViewById(R.id.excerpt);
            date = (TextView)itemView.findViewById(R.id.date);
        }

    }

    public class CircleTransform implements Transformation {
        @Override
        public Bitmap transform(Bitmap source) {
            int size = Math.min(source.getWidth(), source.getHeight());
            int x = (source.getWidth() - size) / 2;
            int y = (source.getHeight() - size) / 2;
            Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
            if (squaredBitmap != source) {
                source.recycle();
            }
            Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());
            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint();
            BitmapShader shader = new BitmapShader(squaredBitmap,
                    BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
            paint.setShader(shader);
            paint.setAntiAlias(true);
            float r = size / 2f;
            canvas.drawCircle(r, r, r, paint);
            squaredBitmap.recycle();
            return bitmap;
        }
        @Override
        public String key() {
            return "circle";
        }
    }
}